/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui (FilePlayer& filePlayer_) : filePlayer (filePlayer_)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
    playbackSlider.setSliderStyle(Slider::LinearHorizontal);
    playbackSlider.setRange(0.0, 1.0);
    playbackSlider.addListener(this);
    addAndMakeVisible(&playbackSlider);
    
    pitchSlider.setSliderStyle(Slider::LinearHorizontal);
    pitchSlider.setRange(0.01, 5.0);
    pitchSlider.setValue(1.0);
    pitchSlider.addListener(this);
    addAndMakeVisible(&pitchSlider);
    
    playbackLabel.setText("Position", dontSendNotification);
    playbackLabel.setJustificationType(Justification::centred);
    addAndMakeVisible(&playbackLabel);
    
    pitchLabel.setText("Pitch", dontSendNotification);
    pitchLabel.setJustificationType(Justification::centred);
    addAndMakeVisible(&pitchLabel);
    
}

FilePlayerGui::~FilePlayerGui()
{
    stopTimer();
    delete fileChooser;
}


//Component
void FilePlayerGui::resized()
{
    playButton.setBounds (0, 0, getHeight() / 3.0, getHeight() / 3.0);
    fileChooser->setBounds (getHeight() / 3.0, 0, getWidth() - (getHeight() / 3.0), getHeight() / 3.0);
    playbackLabel.setBounds (0, getHeight() / 3.0, getWidth() / 5.0, getHeight() / 3.0);
    playbackSlider.setBounds (getWidth() / 5.0, getHeight() / 3.0, (getWidth() / 5.0) * 4.0, getHeight() / 3.0);
    pitchLabel.setBounds (0, (getHeight() / 3.0) * 2.0, getWidth() / 5.0, getHeight() / 3.0);
    pitchSlider.setBounds (getWidth() / 5.0, (getHeight() / 3.0) * 2.0, (getWidth() / 5.0) * 4.0, getHeight() / 3.0);
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        filePlayer.setPlaying(!filePlayer.isPlaying());
        
        if (filePlayer.isPlaying())
            startTimer(250);
        else
        {
            stopTimer();
            playbackSlider.setValue(0.0);
        }
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
                filePlayer.loadFile(audioFile);
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}

// Slider Listener
void FilePlayerGui::sliderValueChanged (Slider* slider)
{
    if (slider == &playbackSlider)
        filePlayer.setPosition(slider->getValue());
    else if (slider == &pitchSlider)
        filePlayer.setPlaybackRate(slider->getValue());
}

// Timer Callback
void FilePlayerGui::timerCallback()
{
    playbackSlider.setValue(filePlayer.getPosition());
}