/*
  ==============================================================================

    FilePlayerGui.h
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#ifndef H_FILEPLAYERGUI
#define H_FILEPLAYERGUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

/**
 Gui for the FilePlayer class
 */
class FilePlayerGui :   public Component,
                        public Button::Listener,
                        public FilenameComponentListener,
                        public Slider::Listener,
                        public Timer
{
public:
    /**
     constructor - receives a reference to a FilePlayer object to control
     */
    FilePlayerGui(FilePlayer& filePlayer_);
    
    /**
     Destructor 
     */
    ~FilePlayerGui();
    
    //Component
    void resized();
    
    //Button Listener
    void buttonClicked (Button* button);
    
    //FilenameComponentListener
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged);
    
    // Slider Listener
    void sliderValueChanged (Slider* slider);
    
    // Timer Callback
    void timerCallback();

private:
    TextButton playButton;
    FilenameComponent* fileChooser;
    Slider playbackSlider;
    Slider pitchSlider;
    Label playbackLabel;
    Label pitchLabel;
    
    FilePlayer& filePlayer;
    
};


#endif  // H_FILEPLAYERGUI
