/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_), filePlayerGui { (audio_.getFilePlayer(0)), (audio_.getFilePlayer(1)) }
{
    setSize (500, 400);

    for (int i = 0; i < NumFilePlayers; i++)
    {
        filePlayerGui[i] = audio.getFilePlayer(i);
        addAndMakeVisible(filePlayerGui[i]);
    }
    
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    for (int i = 0; i < NumFilePlayers; i++)
        filePlayerGui[i].setBounds (0, i * 60.0, getWidth(), 60);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

